VERSION = $(shell git describe --long --tags)

build:
	GOOS=linux GOARCH=amd64 go build . 
pack: build
	zip evento-$(VERSION).zip evento
