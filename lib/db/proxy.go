package db

import(
    "time"
)

type Lodger interface {
    // Get a list of stores
    List() []Store

    // 
    Get(key string) (*Store,error)

    // Save key to store
    Save(key []string , store string) error

    // Updates store status
    Update(store Store) error
}

type Store struct {
    ID string `json:"id"`
    // Host for the store
    Host string `json:"host"`
    //  Flag if store is up or not
    Up   bool   `json:"up"`
    // Last ping from the store
    LastPing time.Time `json:"last_ping"`
    // No more streams or events
    Closed bool `json:"closed"`
}
