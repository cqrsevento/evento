package db

import (
	"encoding/binary"
)

func ByteInt(bytes []byte) uint64 {
	return binary.BigEndian.Uint64(bytes)
}

func IntByte(v uint64) []byte {
	b := make([]byte, binary.MaxVarintLen64)
	binary.BigEndian.PutUint64(b, v)
	return b
}
