package db

import(
    "log"
    badger "github.com/dgraph-io/badger"
    "fmt"
    "errors"
    "encoding/json"
)

const(
    PROXY_FOLDER = "evento_proxy"
    ROUTE_KEY_PREFIX = "k#"
    SERVER_PREFIX = "s#"
)

var(
    ErrRouteKeyNotFound = errors.New("Route key not found")
)



type BadgerLodger struct {
    db *badger.DB
    servers []Store
}

type BadgerConf struct {
    Path string
}

func NewBadgerLodger(conf BadgerConf) *BadgerLodger {
  db, err := badger.Open(badger.DefaultOptions(conf.Path + PROXY_FOLDER ))
  if err != nil {
	  log.Fatal(err)
  }

  var bl BadgerLodger

  bl.db = db

  return &bl
}

func (bl *BadgerLodger) Close() {
    bl.db.Close()
}

func (bl *BadgerLodger) List() []Store {
    servers := make([]Store,0)

    bl.db.View(func(txn *badger.Txn) error {
      opts := badger.DefaultIteratorOptions
      opts.PrefetchSize = 10
      it := txn.NewIterator(opts)
      defer it.Close()
      prefix := []byte(SERVER_PREFIX)
      for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
          item := it.Item()
          fmt.Println(string(item.Key()))
          storeJson, err := item.ValueCopy(nil)
          var store Store
          err = json.Unmarshal(storeJson,&store)
          if err != nil {
              return err
          }
          servers = append(servers,store)
      }
      return nil
    })

    return servers
}

    // Register id and host
func (bl *BadgerLodger) Register(id , host string) error {
    return nil
}

func buildKey(id string) string {
    return ROUTE_KEY_PREFIX + id
}

func buildServerKey(id string) string {
    return SERVER_PREFIX + id
}

func (bl *BadgerLodger) Save(keys []string, server string) error {
    serverKey := buildServerKey(server)

    err := bl.db.Update(func(txn *badger.Txn) error {
        for _, k := range keys {
            if k == "" {
                continue
            }

            routeKey := buildKey(k)
            fmt.Println("routingkey:",routeKey)
            fmt.Println("serverKey:",serverKey)
            fmt.Println("----")
            _, err := txn.Get([]byte(routeKey))
            if err == badger.ErrKeyNotFound {
                err = txn.Set([]byte(routeKey),[]byte(serverKey))
                if err != nil {
                    return err
                }
            }
        }
        return nil
    })

    return err
}

// Gets host for routekey
func (bl *BadgerLodger) Get(routeKey string) (*Store,error) {
    var store Store
    err := bl.db.View(func(txn *badger.Txn) error {
        item, err := txn.Get([]byte(buildKey(routeKey)))
        if err != nil {
            if err == badger.ErrKeyNotFound {
                return ErrRouteKeyNotFound
            }
            return err
        } else {
            serverKey, err := item.ValueCopy(nil)
            if err != nil {
                return err
            }

            storeItem , err := txn.Get(serverKey)
            if err != nil {
                if err == badger.ErrKeyNotFound {
                    return ErrRouteKeyNotFound
                }
                return err
            }

            storeJson, err := storeItem.ValueCopy(nil)
            if err != nil {
                return err
            }

            err = json.Unmarshal(storeJson, &store)
            if err != nil {
                return err
            }
        }
        return nil
    })
    // Check if error on
    if err != nil {
        return nil, err
    }

    return &store,nil
 }
    // Updates store status
 func (bl *BadgerLodger)  Update(store Store) error {
    serverKey := buildServerKey(store.ID)
    b, err := json.Marshal(store)
    if err != nil {
        return err
    }

    err = bl.db.Update(func(txn *badger.Txn) error {
        err := txn.Set([]byte(serverKey), b)
        return err
    })

     return err
 }
