package db

import (
	"errors"
	"github.com/rs/xid"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"

	"sync"
	"time"
)

const (
	DB_FOLDER    = "eventodb"
	LOG_STREAM   = "_all"
	DELETE_BATCH = 100000
)

var (
	StreamNotFound = errors.New("Stream does not exist")
	StreamExist    = errors.New("Stream already exist")
	FailedLock     = errors.New("Version different to lock")
	EventExist     = errors.New("Event ID already exist in store")
	EventNotFound  = errors.New("Event not found")
)

type LevelStore struct {
	ID string

	db           *leveldb.DB
	storeLock    *sync.Mutex
	purgeChannel chan KeyVal

	streamsLock *sync.Mutex
	streams     map[string]*StoreStream
}

type KeyVal struct {
	Key   []byte
	Value []byte
	Opt   string
}

func NewLevelStore() *LevelStore {
	var ls LevelStore
	ls.streams = make(map[string]*StoreStream)
	ls.streamsLock = &sync.Mutex{}
	ls.storeLock = &sync.Mutex{}
	ls.purgeChannel = make(chan KeyVal, 1000)

	return &ls
}

func (l *LevelStore) Init(conf DatabaseConf) error {
	db, err := leveldb.OpenFile(conf.Path+DB_FOLDER, nil)
	if err != nil {
		panic(err)
	}

	if conf.ID == "" {
		conf.ID = xid.New().String()
	}

	l.db = db

	t, err := l.db.OpenTransaction()
	defer t.Commit()
	if err != nil {
		panic(err)
	}

	val, err := t.Get([]byte(ID_KEY), nil)
	if err == leveldb.ErrNotFound {
		err = t.Put([]byte(ID_KEY), []byte(conf.ID), &opt.WriteOptions{Sync: true})
	} else {
		if err != nil {
			panic(err)
		}

		if conf.SetID && string(val) != conf.ID {
			panic("Trying to override server ID")
		}
	}

	// Purge every second if possible
	go func() {
		for {
			l.Purge()
			time.Sleep(time.Second * 60)
		}
	}()

	return err
}

func (l LevelStore) GetID() string {
	val, err := l.db.Get([]byte(ID_KEY), nil)
	if err == leveldb.ErrNotFound {
		panic("No ID set")
	}

	return string(val)
}

func (l LevelStore) Close() {
	l.db.Close()
}

func (l LevelStore) Version(stream string) uint64 {
	storeStream, _, _ := l.StreamExist(stream)

	if storeStream == nil {
		return 0
	} else {
		return storeStream.Version
	}
}

func (l *LevelStore) Read(stream string, version uint64) (*StoreEvent, error) {
	val, err := l.db.Get(StreamKey(stream, version), nil)
	if err != nil {
		return nil, EventNotFound
	}

	stream, version, event := DecodeEvent(StreamKey(stream, version), val)

	if event.Link {
		s, v := ParseStreamKey([]byte(event.Key))
		event, err := l.Read(s, v)
		if err != nil {
			return nil, err
		}
		event.LinkVersion = version
		return event, err
	} else {
		event.Stream = stream
		event.Version = version
	}

	return &event, err
}

func (l *LevelStore) GetEvent(id string) (*StoreEvent, error) {
	val, err := l.db.Get(EncodeID(id), nil)
	if err != nil {
		if err == leveldb.ErrNotFound {
			return nil, EventNotFound
		} else {
			return nil, err
		}
	}
	eventData, err := l.db.Get(val, nil)
	if err != nil {
		if err == leveldb.ErrNotFound {
			return nil, EventNotFound
		} else {
			return nil, err
		}
	}
	stream, version, event := DecodeEvent(val, eventData)
	event.Stream = stream
	event.Version = version

	return &event, nil
}

func (l LevelStore) RangeAll(from, to uint64) ([]StoreEvent, error) {
	return l.Range(LOG_STREAM, from, to)
}

func (l LevelStore) Range(stream string, from, to uint64) ([]StoreEvent, error) {
	events := make([]StoreEvent, 0)
	fromB := RangeBytes(stream, from)
	toB := RangeBytes(stream, to+1)
	ra := &util.Range{Start: fromB, Limit: toB}
	iter := l.db.NewIterator(ra, nil)
	defer iter.Release()
	for iter.Next() {
		_, _, event := DecodeEvent(iter.Key(), iter.Value())
		if event.Link {
			s, v := ParseStreamKey([]byte(event.Key))
			eventLink, err := l.Read(s, v)
			if err == nil {
				eventLink.LinkVersion = event.Version
				eventLink.Stream = s
				events = append(events, *eventLink)
			}
		} else {
			event.Stream = stream
			events = append(events, event)
		}
	}
	return events, nil
}

func RangeBytes(stream string, n uint64) []byte {
	return append([]byte(STREAM_PREFIX+stream), IntByte(n)...)
}

// Store stores events into store
func (l LevelStore) Store(stream string, events []StoreEvent, opt *StoreOpt) (uint64, error) {
	if opt == nil {
		opt = &StoreOpt{}
	}

	for i, _ := range events {
		events[i].GenID()
	}

	// check if stream exist
	l.storeLock.Lock()
	defer l.storeLock.Unlock()

	logStream, _, _ := l.StreamExist(LOG_STREAM)
	if logStream == nil {
		logStream = &StoreStream{Name: LOG_STREAM, Created: time.Now().Unix()}
	}

	storeStream, streamExist, err := l.StreamExist(stream)
	if storeStream == nil {
		storeStream = &StoreStream{Name: stream, Created: time.Now().Unix()}
	}

	if opt.MustExist {
		if !streamExist && err != nil {
			return storeStream.Version, StreamNotFound
		}
	}

	if opt.MustCreate {
		if streamExist {
			return storeStream.Version, StreamExist
		}
	}

	if opt.Lock > 0 && opt.Lock != storeStream.Version {
		return storeStream.Version, FailedLock
	}

	keys := make([]KeyVal, 0)
	nextVersion := storeStream.Version
	nextLogVersion := logStream.Version

	now := time.Now().Unix()
	for _, e := range events {
		if l.ExistID(e.Data.ID, opt.NoIdem) {
			if opt.KeepOrder {
				return storeStream.Version, EventExist
			} else {
				continue
			}
		} else {

			nextVersion = nextVersion + 1
			nextLogVersion = nextLogVersion + 1
			idKey := EncodeID(e.Data.ID)
			e.Created = now

			k, v := EncodeEvent(stream, nextVersion, e)
			// Create log
			logEvent := StoreEvent{
				Link:    true,
				Key:     k,
				Created: now,
			}

			keys = append(keys, KeyVal{Key: k, Value: v})

			// TTL added to events
			if opt.TTL > 0 {
				ttlKey := TTLKey(e.Data.ID, uint64(now)+opt.TTL)
				keys = append(keys, KeyVal{Key: ttlKey, Value: k})
			}
			// Only unique ID events
			keys = append(keys, KeyVal{Key: idKey, Value: k})
			// Store event and key linked to event if not a projection
			if !opt.Projection {
				logKey, logValue := EncodeEvent(LOG_STREAM, nextLogVersion, logEvent)
				keys = append(keys, KeyVal{Key: logKey, Value: logValue})
			}
		}
	}

	if len(keys) == 0 {
		return storeStream.Version, EventExist
	}

	// add stream data key
	storeStream.Version = nextVersion
	streamKey, streamValue := StreamDataEncode(*storeStream)
	keys = append(keys, KeyVal{Key: streamKey, Value: streamValue})

	// save log stream
	if !opt.Projection {
		logStream.Version = nextLogVersion
		logStreamKey, logStreamValue := StreamDataEncode(*logStream)
		keys = append(keys, KeyVal{Key: logStreamKey, Value: logStreamValue})
	}

	// save into timestamp stream

	l.storeVals(keys, opt.Unsafe)

	return storeStream.Version, nil
}

func (l LevelStore) ExistID(id string, noCheck bool) bool {
	if noCheck {
		return false
	}

	val, err := l.db.Get(EncodeID(id), nil)
	if err != nil {
		if err == leveldb.ErrNotFound {
			return false
		} else {
			return true
		}
	}

	if len(val) > 0 {
		return true
	}

	return true
}

func (l LevelStore) StreamExist(stream string) (*StoreStream, bool, error) {
	exist := false
	l.streamsLock.Lock()
	storeStream, existInCache := l.streams[stream]
	l.streamsLock.Unlock()

	if !existInCache {
		dat, err := l.db.Get([]byte(STREAM_NAME_PREFIX+stream), nil)
		if err != nil {
			return nil, exist, err
		} else {
			storeStream := StreamDataDecode([]byte(STREAM_NAME_PREFIX+stream), dat)
			exist = true
			l.streamsLock.Lock()
			l.streams[stream] = &storeStream
			l.streamsLock.Unlock()
			return &storeStream, exist, err
		}
	} else {
		exist = existInCache
	}

	return storeStream, exist, nil
}

func (l LevelStore) StreamVersion(stream string) (uint64, error) {
	val, err := l.db.Get([]byte(STREAM_NAME_PREFIX+stream), nil)
	if err != nil {
		return 0, err
	}
	s := StreamDataDecode([]byte(STREAM_NAME_PREFIX+stream), val)
	return s.Version, err
}

func (l LevelStore) storeVals(kv []KeyVal, unsafe bool) error {
	batch := new(leveldb.Batch)
	for _, k := range kv {
		batch.Put(k.Key, k.Value)
	}
	return l.db.Write(batch, &opt.WriteOptions{Sync: !unsafe})
}

func (l LevelStore) deleteKeys(keys [][]byte, unsafe bool) error {
	batch := new(leveldb.Batch)
	for _, k := range keys {
		batch.Delete(k)
	}
	return l.db.Write(batch, nil)
}

func (l LevelStore) Purge() error {
	now := uint64(time.Now().Unix())
	until := TTLKey("", now)
	ra := &util.Range{Start: []byte(TTL_INDEX), Limit: until}
	iter := l.db.NewIterator(ra, nil)
	defer iter.Release()
	keys := make([][]byte, 0)
	count := 0
	for iter.Next() {
		keys = append(keys, iter.Key())
		keys = append(keys, iter.Value())
		l.db.Delete(iter.Value(), nil)
		l.db.Delete(iter.Key(), nil)
		count = count + 1
		if count >= DELETE_BATCH {
			break
		}
	}
	// Compact DB after delete
	l.db.CompactRange(util.Range{})
	return nil
}
