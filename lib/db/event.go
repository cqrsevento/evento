package db

import(
    "encoding/json"
    "github.com/rs/xid"
    "encoding/binary"
    "fmt"
)

const(
    DELETED_EVENT = "D"
    ID_KEY = "i$"
    IS_LINK = "l"
    NO_LINK = "e"
)

type StoreEvent struct {
    Stream string
    Link   bool
    Key    []byte
    Created int64
    LinkVersion uint64
    Version uint64
    Data EventData
}

func NewStoreEvent(stream,id, eventType string,data interface{}) StoreEvent{
    storeEvent := StoreEvent{
        Stream: stream,
        Data: EventData{
            ID: id,
            Type: eventType,
            Data: data,
        },
    }
    return storeEvent
}

type EventData struct {
    ID string `json:"i"`
    Type string `json:"t,omitempty"`

    Data interface{} `json:"d,omitempty"`

    Link string `json:"l,omitempty"`
    LinkID string `json:"r,omitempty"`
}

func (s *StoreEvent) GenID() {
    if s.Data.ID == "" {
        s.Data.ID = xid.New().String()
    }
}

func EncodeID(id string) []byte {
    return []byte(ID_KEY + id)
}


// EncodeEvent created key value for event
func EncodeEvent(name string, version uint64, e StoreEvent) ([]byte,[]byte){
    streamKey := StreamKey(name,version)
    ts := IntByte(uint64(e.Created))
    buf := make([]byte,0)
    buf = append(buf,ts...)
    if e.Link {
        buf = append(buf,[]byte(IS_LINK)...)
        buf = append(buf,e.Key...)
    }else{
        buf = append(buf,[]byte(NO_LINK)...)
        bData, err  := json.Marshal(e.Data)
        if err != nil {
            fmt.Println(err)
        }
        buf = append(buf,bData...)
    }
    return streamKey,buf
}

func DecodeEvent(key []byte, val []byte) (string,uint64,StoreEvent) {
    event := StoreEvent{}
    name, version := ParseStreamKey(key)
    event.Stream = name
    event.Version = version
    event.Created = int64(ByteInt(val[:binary.MaxVarintLen64]))
    isLink := val[binary.MaxVarintLen64]
    if string(isLink) == IS_LINK {
        event.Link = true
    }
    if !event.Link {
        json.Unmarshal(val[binary.MaxVarintLen64+1:],&event.Data)
    }else{
        event.Key = val[binary.MaxVarintLen64+1:]
    }
    return name,version,event
}


func (e StoreEvent) Valid() error {
    return nil
}
