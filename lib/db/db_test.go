package db

import (
	"fmt"
	"github.com/syndtr/goleveldb/leveldb/util"
	"testing"
	"time"
	//     "github.com/rs/xid"
	//    "encoding/binary"
)

func TestOrder(t *testing.T) {
	ls := NewLevelStore()
	ls.Init(DatabaseConf{})
	defer ls.db.Close()

	nameStream := "t"

	fmt.Println("Start Version")
	fmt.Println(ls.StreamVersion(nameStream))
	t0 := time.Now()
	for i := 1; i <= 1000; i++ {
		events := []StoreEvent{StoreEvent{Created: time.Now().Unix(), Data: EventData{ID: "", Type: "testing", Data: map[string]int{"val": i}}}}
		fmt.Println(ls.Store(nameStream, events, &StoreOpt{Unsafe: true, NoIdem: true, TTL: 10}))
	}
	fmt.Println("Version")
	fmt.Println(ls.StreamVersion(nameStream))

	to := append([]byte(STREAM_PREFIX+nameStream), IntByte(11)...)
	ra := &util.Range{Start: []byte(STREAM_PREFIX + nameStream), Limit: []byte(to)}
	iter := ls.db.NewIterator(ra, nil)
	for iter.Next() {
		fmt.Println(DecodeEvent(iter.Key(), iter.Value()))
	}
	t1 := time.Now()
	fmt.Println(t1.Sub(t0))
	iter.Release()

	fmt.Println("Purge:")
	fmt.Println(ls.Purge())
}
