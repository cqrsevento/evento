package db

import (
	"encoding/binary"
	"strings"
)

const (
	STREAM_PREFIX      = "s$"
	STREAM_NAME_PREFIX = "n$"
	CHANGE_LOG         = "c$"
	TTL_INDEX          = "t$"
)

type StoreStream struct {
	Name    string `json:"name"`
	Created int64  `json:"created"`
	Version uint64 `json:"version"`
}

func StreamDataDecode(key, val []byte) StoreStream {
	s := StoreStream{}
	s.Name = string(key[len(STREAM_NAME_PREFIX):])
	s.Version = ByteInt(val[:binary.MaxVarintLen64])
	s.Created = int64(ByteInt(val[binary.MaxVarintLen64:]))
	return s
}

// StreamDataEncode returns key and value for stream data
func StreamDataEncode(stream StoreStream) ([]byte, []byte) {
	data := append(IntByte(stream.Version), IntByte(uint64(stream.Created))...)
	return []byte(STREAM_NAME_PREFIX + stream.Name), data
}

// StreamKey creates stream key
func StreamKey(name string, version uint64) []byte {
	return append([]byte(STREAM_PREFIX+name), IntByte(version)...)
}

// TTLKey builds key for ttl index
func TTLKey(id string, ttl uint64) []byte {
	k := append([]byte(TTL_INDEX), IntByte(ttl)...)
	k = append(k, []byte(id)...)
	return k
}

func ParseTTLKey(key []byte) uint64 {
	return ByteInt(key[len(TTL_INDEX):binary.MaxVarintLen64])
}

// ParseStreamKey parses key and returns stream name and version
func ParseStreamKey(key []byte) (string, uint64) {
	l := len(key) - binary.MaxVarintLen64
	parts := strings.Split(string(key[:l]), "$")
	if len(parts) > 0 {
		return parts[1], ByteInt(key[l:])
	} else {
		return "", ByteInt(key[l:])
	}
}

// ValidStreamName validates stream name.
func ValidStreamName(name string) error {
	return nil
}
