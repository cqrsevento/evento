package db

type DatabaseConf struct {
	ID    string
	SetID bool
	Path  string
}

type StoreOpt struct {
	// Projection, does not save in main log
	Projection bool `json:"projection"`

	// Check lock version in stream
	Lock uint64 `json:"lock"`

	// Stream does not have to exist
	MustCreate bool `json:"create"`

	// Stream must exist to save event
	MustExist bool `json:"exist"`

	KeepOrder bool `json:"keep_order"`

	Unsafe bool `json:"unsafe"`

	NoIdem bool `json:"no_idem"`

	TTL uint64 `json:"ttl"`
}

// Db is event store general interface No direct DELETE, just purge old events with TTL
type Db interface {
	Init(conf DatabaseConf) error
	GetID() string
	Store(stream string, events []StoreEvent, opt *StoreOpt) (uint64, error)
	Read(stream string, version uint64) (*StoreEvent, error)
	Range(stream string, from uint64, to uint64) ([]StoreEvent, error)
	RangeAll(from, to uint64) ([]StoreEvent, error)
	GetEvent(id string) (*StoreEvent, error)
	Version(stream string) uint64
	// Optional purge events with TTL
	Purge() error
	Close()
	//    Close() error
}
