package main

import (
	"fmt"
	"github.com/docopt/docopt-go"
	"gitlab.com/cqrsevento/evento/lib/db"
	"gitlab.com/cqrsevento/evento/proxy"
	"gitlab.com/cqrsevento/evento/replica"
	"gitlab.com/cqrsevento/evento/server"
	"gitlab.com/cqrsevento/evento/store"
	"log"
	"os"
	"os/signal"
)

const (
	VERSION = "v1.0.1"
)

type Args struct {
	Proxy        bool
	ProxyPort    string
	EventoStores []string

	Version    bool
	Dev        bool
	Server     bool
	ServerPort string
	Replica    bool
	Host       string
	ID         string
	SetID      bool
	Path       string
}

func parseArguments(rawArgs map[string]interface{}) Args {
	args := Args{}

	if rawArgs["version"].(bool) {
		args.Version = true
	}

	if rawArgs["replica"].(bool) {
		args.Replica = true
		args.ServerPort = rawArgs["--api-port"].(string)
		args.Host = rawArgs["<host>"].(string)
	}

	if rawArgs["proxy"].(bool) {
		args.Proxy = true
		args.ProxyPort = rawArgs["--proxy-port"].(string)
		args.EventoStores = rawArgs["<servers>"].([]string)
	}

	if rawArgs["server"].(bool) {
		args.Server = true
		args.ServerPort = rawArgs["--api-port"].(string)
	}

	if rawArgs["--dev"].(bool) {
		args.Dev = true
	}

	if rawArgs["--id"] != nil {
		args.ID = rawArgs["--id"].(string)
		args.SetID = true
	}

	args.Path = rawArgs["--data-path"].(string)

	return args
}

func waitExit() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// wait for os.Signal
	for e := range c {
		fmt.Println(e)
		break
	}
}

func main() {
	usage := `
    Evento
    Usage:
        evento proxy  [ --dev --id=<id> --data-path=<path> --proxy-port=<port>] <servers>...
        evento server [ --dev --id=<id> --data-path=<path> --api-port=<port>]
        evento replica <host> [ --dev --id=<id> --data-path=<path> --api-port=<port>]
        evento version

    Options:
        --proxy-port=<port>     Proxy HTTP port [default: 9420]
        --api-port=<port>       API HTTP port [default: 8420]
        --dev                   Development flag.
        --id=<id>               Define store ID if not assigned before.
        --data-path=<path>      Define data path. [default: ./]
    `

	arguments, err := docopt.ParseDoc(usage)
	if err != nil {
		log.Fatal(err)
	}

	args := parseArguments(arguments)
	if args.Version {
		fmt.Println("Evento, version:", VERSION)
	} else {
		if args.Proxy {
			lodger := db.NewBadgerLodger(db.BadgerConf{Path: args.Path})
			defer lodger.Close()

			p := proxy.NewProxy(lodger, args.EventoStores)
			err := p.Init()
			if err != nil {
				log.Fatal("Failed to init proxy: ", err.Error())
			}

			proxy.Run(proxy.ServerConf{ProxyPort: args.ProxyPort, Dev: args.Dev}, p)
			waitExit()
			os.Exit(1)
		} else {
			levelStore := db.NewLevelStore()
			err = levelStore.Init(db.DatabaseConf{ID: args.ID, Path: args.Path, SetID: args.SetID})
			if err != nil {
				panic(err)
			}
			defer levelStore.Close()

			store.Init(levelStore)

			fmt.Println("Store ID:", levelStore.GetID())

			if args.Server {
				go server.Run(
					server.ServerConf{
						Port: args.ServerPort,
						Dev:  args.Dev,
					})
				waitExit()
				os.Exit(1)
			}

			if args.Replica {
				replicator := replica.Replicate(args.Host)
				go replicator.Run()
				go server.Run(
					server.ServerConf{
						Port:    args.ServerPort,
						Dev:     args.Dev,
						Replica: true,
					})
				waitExit()
				os.Exit(1)
			}
		}
	}
}
