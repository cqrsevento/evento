package proxy

import(
    "github.com/gin-gonic/gin"
    "fmt"
)

var p *Proxy

type ServerConf struct {
    Version   string `json:"version"`
    ProxyPort string `json:"proxy_port"`
    Dev  bool   `json:"dev"`
}

func Run(conf ServerConf, proxy *Proxy) {
    if !conf.Dev {
        gin.SetMode(gin.ReleaseMode)
    }

    p = proxy

    server := gin.New()
    server.Use(gin.Recovery())


    if conf.Dev {
        fmt.Println("logging..")
        server.Use(gin.Logger())
    }

    server.GET("/ping",PingHandler)
    server.GET("/route/:key",RouteHandler)
    server.GET("/servers",ListServersHandler)

    server.Run(":"+ conf.ProxyPort)
}

// RouteHandler resolve all streams or keys to a server
func RouteHandler(c *gin.Context){
    key := c.Param("key")
    stream := c.Query("stream")

    fmt.Println(key,stream)
    server, err := p.Route(key,stream)
    if err != nil {
        c.JSON(404,map[string]string{ "server" : "", "error" : err.Error()})
    }else {
        c.JSON(200,map[string]string{ "server" : server, "error" : ""})
    }
}

// List all proxied servers
func ListServersHandler(c *gin.Context){
    servers := p.ListServer()
    c.JSON(200,map[string]interface{}{ "servers" : servers})
}

func PingHandler(c *gin.Context){
    c.JSON(200,map[string]interface{}{ "ok" : true})
}
