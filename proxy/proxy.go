package proxy

import (
	"errors"
	"fmt"
	evento "gitlab.com/cqrsevento/evento/client"
	"gitlab.com/cqrsevento/evento/lib/db"
	"sync"
	"time"
)

type Proxy struct {
	lock        sync.Mutex
	lodger      db.Lodger
	servers     []string
	stores      []db.Store
	storeNumber int
}

func NewProxy(db db.Lodger, servers []string) *Proxy {
	var proxy Proxy
	proxy.servers = servers
	proxy.lodger = db
	proxy.storeNumber = 1
	return &proxy
}

func (p *Proxy) Init() error {
	// Close all stores
	stores := p.lodger.List()
	for _, store := range stores {
		store.Closed = true
		err := p.lodger.Update(store)
		if err != nil {
			return err
		}
	}

	for _, server := range p.servers {
		store, err := pingStore(server)
		if err != nil {
			return err
		}
		err = p.lodger.Update(*store)
		if err != nil {
			return err
		}

	}

	stores = p.lodger.List()
	for i, store := range stores {
		fmt.Println("Store", i)
		fmt.Println(" ID: ", store.ID)
		fmt.Println(" Host: ", store.Host)
		fmt.Println(" Closed: ", store.Closed)
		p.stores = append(p.stores, store)
	}
	return nil
}

func (p *Proxy) GetServer() (db.Store, error) {
	p.lock.Lock()
	defer p.lock.Unlock()

	count := 0
	for {
		fmt.Println(p.storeNumber, len(p.stores))
		if store := p.stores[p.storeNumber%len(p.stores)]; !store.Closed {
			p.storeNumber = p.storeNumber + 1
			return store, nil
		} else {
			p.storeNumber = p.storeNumber + 1
		}
		count = count + 1

		if count > len(p.stores) {
			break
		}
	}

	return db.Store{}, errors.New("All stores are closed")
}

// Route returns host where the key should be routed
func (p *Proxy) Route(key, stream string) (string, error) {
	if stream == "" {
		stream = key
	}

	if stream == "_all" {
		return "", errors.New("_all stream cannot be routed")
	}

	// Try to get key or stream from store
	storeKey, errKey := p.lodger.Get(key)
	storeStream, errStream := p.lodger.Get(stream)

	if errKey == nil && errStream == db.ErrRouteKeyNotFound {
		err := p.lodger.Save([]string{stream}, storeKey.ID)
		if err != nil {
			return "", err
		}
	}

	if errKey == db.ErrRouteKeyNotFound && errStream == db.ErrRouteKeyNotFound {
		selectedStore, err := p.GetServer()
		if err != nil {
			return "", err
		}
		err = p.lodger.Save([]string{key, stream}, selectedStore.ID)
		if err != nil {
			return "", err
		}

		store, err := p.lodger.Get(key)
		if err != nil {
			return "", err
		}

		return store.Host, err
	}

	if errKey == db.ErrRouteKeyNotFound && errStream == nil {
		err := p.lodger.Save([]string{key}, storeStream.ID)
		if err != nil {
			return "", err
		}

		return storeStream.Host, err
	}

	return storeStream.Host, errStream
}

func pingStore(host string) (*db.Store, error) {
	var store db.Store
	cli := evento.NewClient(host)

	if err := cli.Ping(); err != nil {
		return nil, errors.New("Failed to connect to server")
	}

	id, err := cli.ID()
	if err != nil {
		return nil, err
	}

	store.ID = id
	store.Host = host
	store.LastPing = time.Now().UTC()
	store.Up = true
	store.Closed = false

	return &store, err
}

func (p *Proxy) ListServer() []db.Store {
	return p.lodger.List()
}
