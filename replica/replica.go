package replica

import(
    "gitlab.com/cqrsevento/evento/client"
    "gitlab.com/cqrsevento/evento/store"
    "time"
    "fmt"
)


var (
    RetryTimes = 10
    FetchEvents = uint64(10000)
    LogStream = "_all"
)

type Replicator struct {
    cli *client.Client
}

func Replicate(host string) *Replicator {
    c := client.NewClient(host)
    for i:= 1 ; i <= RetryTimes ; i ++ {
        fmt.Println("Connecting to host...",host)
        err := c. Ping()
        if err == nil {
            fmt.Println("Ok...")
            break
        }else{
            fmt.Println("Failed:",err)
        }
        time.Sleep(time.Second * 30)
    }

    var re Replicator
    re.cli = c
    return &re
}

func (r Replicator) Run() {
    // Load status :=
    from := store.Version(LogStream)
    fmt.Println("Starting from:",from)
    for {
        to , _:= r.cli.Version(LogStream)
        if from == to {
            time.Sleep(time.Second * 1)
        }

        events, err := r.cli.RangeAll(from+1,from+FetchEvents)
        if err != nil {
            fmt.Println("Failed to get events..",err)
            time.Sleep(time.Second * 30)
        }
        for _, e := range events {
            from = e.LinkVersion
            res := store.SaveEvent(e.Stream,e,&store.SaveOpt{ Unsafe : true})
            if res.Error {
                panic(res.Msg)
            }
        }

        from = store.Version(LogStream)
        time.Sleep(time.Millisecond * 100)
    }
}
