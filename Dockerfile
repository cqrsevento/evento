FROM golang AS build
RUN mkdir /go/src/evento/
ENV GOPATH /go
WORKDIR /go/src/evento/
COPY . .
RUN GOOS=linux GOARCH=amd64 go build .

FROM ubuntu
WORKDIR /
COPY --from=build /go/src/evento/evento .
ENTRYPOINT [ "/evento" ]
