package server

type Event struct {
    ID string `json:"-"`
    Stream string `json:"-"`
    Type string `json:"type"`
    Data  interface{} `json:"data"`
}
