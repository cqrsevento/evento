package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
	es "gitlab.com/cqrsevento/evento/store"
	"strconv"
)

type ServerConf struct {
	Version   string `json:"version"`
	ProxyPort string `json:"proxy_port"`
	Port      string `json:"port"`
	Dev       bool   `json:"dev"`
	Replica   bool   `json:"replica"`
	Proxy     bool   `json:"proxy"`
}

func Run(conf ServerConf) {
	fmt.Println("Starting HTTP server, port => :" + conf.Port)

	if !conf.Dev {
		gin.SetMode(gin.ReleaseMode)
	}

	server := gin.New()
	server.Use(gin.Recovery())

	if conf.Dev {
		fmt.Println("logging..")
		server.Use(gin.Logger())
	}

	if conf.Proxy {
	} else {
		if !conf.Replica {
			server.POST("/stream/:stream/:type", EventHandler)
			server.POST("/stream/:stream/:type/:id", EventHandler)
		}

		server.GET("/ping", PingHandler)
		server.GET("/stream/:stream", VersionHandler)
		server.GET("/stream/:stream/:version", EventVersionHandler)
		// Range stream using version
		server.GET("/range/:stream/:from/:to", RangeHandler)
		// Range event based on creation time
		server.GET("/all/:from/:to", TimeRangeHandler)
		server.GET("/event/:id", EventIdHandler)
		server.GET("/id", ServerIDHandler)
		server.GET("/purge", PurgeHandler)
		server.Run(":" + conf.Port)
	}
}

func ServerIDHandler(c *gin.Context) {
	id := es.GetID()
	c.JSON(200, map[string]string{"id": id})
}

func EventVersionHandler(c *gin.Context) {
	sStream := c.Param("stream")
	sVersion := c.Param("version")

	ts, err := strconv.ParseUint(sVersion, 10, 64)
	if err != nil {
		c.JSON(400, map[string]string{"error": "Invalid range"})
		return
	}

	event, err := es.ReadEvent(sStream, ts)
	if err != nil {
		c.JSON(400, map[string]string{"error": err.Error()})
		return
	}

	c.JSON(200, event)
}

func PurgeHandler(c *gin.Context) {
	go es.Purge()
}

func EventIdHandler(c *gin.Context) {
	id := c.Param("id")
	event, err := es.GetEvent(id)
	if err != nil {
		c.JSON(404, map[string]interface{}{"error": err.Error()})
		return
	}

	c.JSON(200, event)
}

func TimeRangeHandler(c *gin.Context) {
	sFrom := c.Param("from")
	sTo := c.Param("to")

	from, err := strconv.ParseUint(sFrom, 10, 64)
	if err != nil {
		c.JSON(400, map[string]string{"error": "Invalid range"})
		return
	}

	to, err := strconv.ParseUint(sTo, 10, 64)
	if err != nil {
		c.JSON(400, map[string]string{"error": "Invalid range"})
		return
	}

	events := es.RangeAll(from, to)

	c.JSON(200, events)
}

func RangeHandler(c *gin.Context) {
	streamName := c.Param("stream")
	sFrom := c.Param("from")
	sTo := c.Param("to")

	from, err := strconv.ParseUint(sFrom, 10, 64)
	if err != nil {
		c.JSON(400, map[string]string{"error": "Invalid range"})
		return
	}

	to, err := strconv.ParseUint(sTo, 10, 64)
	if err != nil {
		c.JSON(400, map[string]string{"error": "Invalid range"})
		return
	}

	r, err := es.Range(streamName, from, to)
	if err != nil {
		c.JSON(400, map[string]interface{}{"error": err.Error()})
		return
	}

	c.JSON(200, r)
}

func VersionHandler(c *gin.Context) {
	streamName := c.Param("stream")
	v := es.Version(streamName)
	c.JSON(200, map[string]interface{}{"version": v})
}

type EventData struct {
	Data interface{}
}

func EventHandler(c *gin.Context) {
	var err error
	streamName := c.Param("stream")

	ttl := uint64(0)
	if sTtl := c.Query("ttl"); sTtl != "" {
		ttl, err = strconv.ParseUint(sTtl, 10, 64)
		if err != nil {
			c.JSON(400, map[string]interface{}{"error": true, "msg": "Invalid ttl value"})
			return
		}
	}

	lock := uint64(0)
	if sLock := c.Query("lock"); sLock != "" {
		lock, err = strconv.ParseUint(sLock, 10, 64)
		if err != nil {
			c.JSON(400, map[string]interface{}{"error": true, "msg": "Invalid lock"})
			return
		}
	}

	projection := false
	if sProjection := c.Query("projection"); sProjection != "" {
		if sProjection == "true" {
			fmt.Println("is projection?")
			projection = true
		}
	}

	exist := false
	if existFlag := c.Query("exist"); existFlag == "true" {
		exist = true
	}

	create := false
	if createFlag := c.Query("create"); createFlag == "true" {
		create = true
	}

	safe := true
	if unsafe := c.Query("unsafe"); unsafe == "true" {
		safe = false
	}

	var data EventData
	err = c.BindJSON(&data)
	if err != nil {
		c.JSON(400, map[string]interface{}{"error": true, "msg": "invalid data"})
		return
	}

	if data.Data == nil {
		c.JSON(400, map[string]interface{}{"error": true, "msg": "invalid data"})
		return
	}

	var event es.Event
	event.ID = c.Param("id")
	event.Type = c.Param("type")
	event.Data = data.Data

	if event.Type == "" {
		c.JSON(400, map[string]interface{}{"error": true, "type": "no type in event"})
		return
	}

	res := es.SaveEvent(streamName, event, &es.SaveOpt{Projection: projection, Lock: lock, MustCreate: create, MustExist: exist, Unsafe: !safe, TTL: ttl})
	if res.Error {
		c.JSON(400, res)
	} else {
		c.JSON(200, res)
	}
}

func PingHandler(c *gin.Context) {
	c.JSON(200, map[string]string{"version": "0.1"})
}
