package store

import (
	"errors"
	"gitlab.com/cqrsevento/evento/lib/db"
	"regexp"
)

const (
	JSON_TYPE = "JSON"
)

var (
	dbase   db.Db
	validID = regexp.MustCompile("^[#@0-9a-zA-Z_\\-]{0,100}$")
)

func Init(database db.Db) {
	dbase = database
}

type SaveOpt struct {
	Projection bool
	Lock       uint64
	MustExist  bool
	MustCreate bool
	Unsafe     bool
	TTL        uint64
}

type Event struct {
	ID          string      `json:"id"`
	Stream      string      `json:"stream"`
	Type        string      `json:"type"`
	Created     int64       `json:"created"`
	Version     uint64      `json:"version"`
	LinkVersion uint64      `json:"link_version"`
	Data        interface{} `json:"data"`
}

type EventRange struct {
	Events []Event `json:"events"`
}

func NewEventRange() *EventRange {
	var r EventRange
	r.Events = make([]Event, 0)
	return &r
}

func Purge() {
	dbase.Purge()
}

func ReadEvent(stream string, version uint64) (*Event, error) {
	e, err := dbase.Read(stream, version)
	if err != nil {
		return nil, err
	}

	event := &Event{
		ID:          e.Data.ID,
		Stream:      e.Stream,
		Type:        e.Data.Type,
		Version:     e.Version,
		Created:     e.Created,
		LinkVersion: e.LinkVersion,
		Data:        e.Data.Data,
	}

	return event, err
}

func GetEvent(id string) (*Event, error) {
	e, err := dbase.GetEvent(id)
	if err != nil {
		return nil, err
	}

	event := &Event{
		ID:          e.Data.ID,
		Stream:      e.Stream,
		Type:        e.Data.Type,
		Version:     e.Version,
		Created:     e.Created,
		LinkVersion: e.LinkVersion,
		Data:        e.Data.Data,
	}

	return event, err
}

func RangeAll(from, to uint64) *EventRange {
	r := NewEventRange()
	events, err := dbase.RangeAll(from, to)
	if err != nil {
		return r
	}

	for _, e := range events {
		event := Event{
			ID:          e.Data.ID,
			Stream:      e.Stream,
			Type:        e.Data.Type,
			Version:     e.Version,
			Created:     e.Created,
			LinkVersion: e.LinkVersion,
			Data:        e.Data.Data,
		}
		r.Events = append(r.Events, event)
	}
	return r
}

func GetID() string {
	return dbase.GetID()
}

func Range(stream string, from uint64, to uint64) (*EventRange, error) {
	r := NewEventRange()
	events, err := dbase.Range(stream, from, to)

	for _, e := range events {
		event := Event{
			ID:          e.Data.ID,
			Stream:      e.Stream,
			Type:        e.Data.Type,
			Version:     e.Version,
			Created:     e.Created,
			LinkVersion: e.LinkVersion,
			Data:        e.Data.Data,
		}
		r.Events = append(r.Events, event)
	}

	return r, err
}

type StoreResponse struct {
	Error   bool   `json:"error"`
	Msg     string `json:"msg"`
	Version uint64 `json:"version"`
}

func SaveEvent(stream string, event Event, opt *SaveOpt) StoreResponse {
	if opt == nil {
		opt = &SaveOpt{}
	}

	storeEvent := db.NewStoreEvent(stream, event.ID, event.Type, event.Data)

	if err := ValidEvent(stream); err != nil {
		return StoreResponse{
			Error: true,
			Msg:   err.Error(),
		}
	}

	// Store event into database
	version, err := dbase.Store(stream, []db.StoreEvent{storeEvent}, &db.StoreOpt{Projection: opt.Projection, MustCreate: opt.MustCreate, MustExist: opt.MustExist, Lock: opt.Lock, Unsafe: opt.Unsafe, TTL: opt.TTL})
	if err != nil {
		return StoreResponse{
			Error: true,
			Msg:   err.Error(),
		}
	} else {
		return StoreResponse{
			Version: version,
		}
	}
}

func ValidEvent(stream string) error {
	if !validID.Match([]byte(stream)) {
		return errors.New("Invalid stream")
	}

	return nil
}

func Version(stream string) uint64 {
	return dbase.Version(stream)
}
