package main

import(
    es "evento/client"
    "evento/store"
    "fmt"
)

func main(){
    stream := "account-01"
    c := es.NewClient("http://localhost:8420")
    fmt.Println(c.Ping())
    events, err := c.Range(stream,0,1230000102)
    fmt.Println(err)
    fmt.Println(events)
    fmt.Println(len(events))
    fmt.Println(events[0],events[len(events)-1])

    v := make(map[uint64]bool)
    for _, e := range events {
        _, exist := v[e.Version]
        if exist {
            panic("Exist!")
        }
        v[e.Version] = true
    }
    fmt.Println(c.Version(stream))

    var d struct {
        Name string `json:"n"`
        Num int `json:"num"`
    }

    for i:= 0 ; i < 100 ; i ++ {
        d.Name = "Diego"
        d.Num = i
        go fmt.Println(c.Save("gocli","Tested",d, &store.SaveOpt{ Unsafe : true}))
    }

    //fmt.Println(c.Read(stream,100))
    fmt.Println(c.Version("askldmalkdams"))
    fmt.Println(c.Save("gocli","Tested",d, &store.SaveOpt{ Unsafe : true, Lock: 3}))
}
