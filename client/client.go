package client

import (
	"errors"
	"fmt"
	"gitlab.com/cqrsevento/evento/store"
	g "gopkg.in/h2non/gentleman.v2"
	"gopkg.in/h2non/gentleman.v2/plugins/body"
	"gopkg.in/h2non/gentleman.v2/plugins/query"
)

type Client struct {
	Host    string
	Pag     int
	isProxy bool
	cli     *g.Client
}

type Context struct {
	RouteKey string
}

func NewContext() *Context {
	return &Context{}
}

func (c *Context) WithRouteKey(k string) *Context {
	if k != "" {
		c.RouteKey = k
	}

	return c
}

func NewClient(host string) *Client {
	var c Client
	c.Host = host
	c.Pag = 1000
	c.cli = g.New()
	c.cli.URL(host)
	return &c
}

func (c *Client) IsProxy() *Client {
	c.isProxy = true
	return c
}

func (c *Client) clearHttpClient() {
	if c.isProxy {
		c.cli = nil
	}
}

func (c *Client) getHttpClient(key, stream string) (*g.Client, error) {
	var err error
	if c.isProxy {
		cli := g.New()
		server, err := c.resolveKey(key, stream)
		if err != nil {
			return nil, err
		}
		cli.URL(server)
		return cli, err
	} else {
		return c.cli, err
	}
}

func (c *Client) Save(ctx *Context, stream, eventType string, data interface{}, opt *store.SaveOpt) (uint64, error) {
	return c.saveEvent(ctx, stream, eventType, "", data, opt)
}

func (c *Client) SaveWithID(ctx *Context, id, stream, eventType string, data interface{}, opt *store.SaveOpt) (uint64, error) {
	return c.saveEvent(ctx, stream, eventType, id, data, opt)
}

func (c *Client) saveEvent(ctx *Context, stream, eventType, id string, data interface{}, opt *store.SaveOpt) (uint64, error) {
	if ctx == nil {
		ctx = NewContext().WithRouteKey(stream)
	}

	cli, err := c.getHttpClient(ctx.RouteKey, stream)
	if err != nil {
		return 0, err
	}

	var d struct {
		Data interface{} `json:"data"`
	}

	req := cli.Request()

	d.Data = data

	path := fmt.Sprintf("/stream/%s/%s", stream, eventType)
	if id != "" {
		path = path + "/" + id
	}

	if opt != nil {
		if opt.Unsafe {
			req.Use(query.Set("unsafe", "true"))
		} else {
			req.Use(query.Set("unsafe", "false"))
		}

		if opt.Lock > 0 {
			req.Use(query.Set("lock", fmt.Sprintf("%d", opt.Lock)))
		}

		if opt.MustExist {
			req.Use(query.Set("exist", "true"))
		}

		if opt.MustCreate {
			req.Use(query.Set("create", "true"))
		}

		if opt.Projection {
			req.Use(query.Set("projection", "true"))
		}
	}

	req.Use(body.JSON(d))
	res, err := req.Method("POST").Path(path).Send()
	if err != nil {
		return 0, err
	}

	respStruct := store.StoreResponse{}
	err = res.JSON(&respStruct)
	if err != nil {
		return 0, err
	}

	if respStruct.Error {
		return 0, errors.New(respStruct.Msg)
	}

	return respStruct.Version, err
}

func (c *Client) Range(stream string, from, to uint64) ([]store.Event, error) {
	cli, err := c.getHttpClient(stream, "")
	if err != nil {
		return []store.Event{}, err
	}

	pages := int((to-from)/uint64(c.Pag)) + 1
	start := from
	events := make([]store.Event, 0)
	for p := 1; p <= pages; p++ {
		req := cli.Request()

		pageEnd := start + uint64(c.Pag)
		if pageEnd >= to {
			pageEnd = to
		}

		path := fmt.Sprintf("/range/%s/%d/%d", stream, start, pageEnd)
		res, err := req.Method("GET").Path(path).Send()
		if err != nil {
			return events, err
		}

		part := store.NewEventRange()
		err = res.JSON(&part)
		if err != nil {
			return events, err
		}
		if len(part.Events) == 0 {
			break
		}
		events = append(events, part.Events...)

		start = start + uint64(c.Pag) + 1
	}

	return events, nil
}

func (c *Client) Version(stream string) (uint64, error) {
	req := c.cli.Request()
	res, err := req.Method("GET").Path("/stream/" + stream).Send()
	if err != nil {
		return 0, err
	}

	var d struct {
		Version uint64 `json:"version"`
	}

	err = res.JSON(&d)
	if err != nil {
		return 0, err
	}

	return d.Version, err
}

func (c *Client) Ping() error {
	req := c.cli.Request()
	res, err := req.Method("GET").Path("/ping").Send()
	if err != nil {
		return err
	}

	if res.StatusCode != 200 {
		return errors.New("Failed to ping server")
	}

	return err
}

func (c *Client) RangeAll(from, to uint64) ([]store.Event, error) {
	req := c.cli.Request()
	path := fmt.Sprintf("/all/%d/%d", from, to)
	res, err := req.Method("GET").Path(path).Send()
	if err != nil {
		return []store.Event{}, err
	}

	part := store.NewEventRange()
	err = res.JSON(&part)
	if err != nil {
		return part.Events, err
	}

	return part.Events, err
}

func (c *Client) ID() (string, error) {
	req := c.cli.Request()

	var id struct {
		ID string `json:"id"`
	}

	res, err := req.Method("GET").Path("/id").Send()
	if err != nil {
		return "", err
	}

	if res.StatusCode != 200 {
		return "", errors.New("Failed to get ID server")
	}

	err = res.JSON(&id)
	if err != nil {
		return "", err
	}

	return id.ID, err
}
