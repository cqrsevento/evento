package client

import(
    "fmt"
    "strconv"
    "testing"
	"gitlab.com/cqrsevento/evento/store"
)

func TestClient(t *testing.T){
    cli := NewClient("http://localhost:9420").IsProxy()

    prefix := "test-"

    for i := 1 ; i < 100 ; i++ {
        stream := prefix + strconv.Itoa(i)
        cli.Save(nil,stream,"testing",3,&store.SaveOpt{ Unsafe :true})
    }

    events, _ := cli.Range("test-3",0,100)
    fmt.Println(len(events))
    fmt.Println(events)
}
