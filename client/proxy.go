package client

import(
    g "gopkg.in/h2non/gentleman.v2"
    "gopkg.in/h2non/gentleman.v2/plugins/query"
)

type ProxyResponse struct {
    Server  string `json:"server"`
    Error   string `json:"error"`
}

func (c *Client) resolveKey(key , stream string) (string,error) {
    req := g.New().URL(c.Host).Request().Path("/route/" + key)
    if stream != "" && stream != key {
        req.Use(query.Set("stream",stream))
    }

    res, err := req.Send()
    if err != nil {
        return "", err
    }

    var pRes ProxyResponse
    err = res.JSON(&pRes)
    if err != nil {
        return "", err
    }

    return pRes.Server,err
}
